<%@ page contentType="text/html;charset=utf-8"%>
<%@ page import="java.sql.*"%>
<html>
<body>
<%
Connection con;
Statement sql;
ResultSet rs;
try{Class.forName("com.mysql.jdbc.Driver").newInstance();}
catch(Exception e){out.print(e);}
try{
    String uri="jdbc:mysql://localhost:3306";                //mysql默认连接端口3306
    con=DriverManager.getConnection(uri,"root","mysql");     //"root"对应MySql用户，"mysql"对应其密码
    sql=con.createStatement();
    rs=sql.executeQuery("use acfun_db");                     //指定数据库
    rs=sql.executeQuery("SELECT * FROM acfun_complex");
    out.print("<table border=2>");
    out.print("<tr>");
    out.print("<th width=100>"+"aid");
    out.print("<th width=100>"+"title");
    out.print("<th width=100>"+"dateinfo");
    out.print("<th width=100>"+"bananas");
    out.print("<th width=100>"+"comments");
    out.print("<th width=100>"+"collects");
    out.print("<th width=100>"+"views");
    out.print("<th width=100>"+"danmus");
    out.print("</tr>");
    while(rs.next()){
        out.print("<tr>");
        out.print("<td>"+rs.getString(1)+"</td>");
        out.print("<td>"+rs.getString(2)+"</td>");
        out.print("<td>"+rs.getString(3)+"</td>");
        out.print("<td>"+rs.getString(4)+"</td>");
        out.print("<td>"+rs.getString(5)+"</td>");
        out.print("<td>"+rs.getString(6)+"</td>");
        out.print("<td>"+rs.getString(7)+"</td>");
        out.print("<td>"+rs.getString(8)+"</td>");
        out.print("</tr>");
    }
    out.print("</table>");
    con.close();
}
catch(SQLException e1){out.print(e1);}
%>
</body>
</html>