#Artical_AcFun
# -*- "毕业设计" -*-

'''
  _**step 1 爬取文章链接(acfun.py)**_ 
 _**step 2 存储在mysql数据库(paplus.py)**_ 
 **_step 3 部署到服务器(web.jsp)
step 3.a 编写web(格式输出数据)
step 3.b 连接数据库到Tomcat_** 
 **_step 4 安卓端获取_** 
'''

day1: 2016/12/04
    学习Python基本语法完毕，开始并完成了爬虫的学习与制作
day2: 2016/12/05
    学习Python的数据库操作，开始并完成了把爬取到的信息存储到MySQL的工作
day3: 2016/12/06
    学习、部署本地Tomcat服务器，开始并完成了部署本地服务器及编写其访问页面的工作
day4: 2016/12/07
    丰富爬虫内容,更新jsp页面；轻量优化爬虫项目
day5: 2016/12/09
    增加评论页爬虫可能，完成Python的评论页爬虫Demo