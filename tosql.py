__author__ = 'maesleung'
# -*- coding:utf-8 -*-
import mysql.connector

from datetime import datetime

#建立连接对象
conn = mysql.connector.connect(user="root", password='mysql', database='acfun_db',
                               charset='utf8')
cur = conn.cursor()

#插入数据
now = datetime.now()
now = now.strftime('%y%m%d%H')

cur.execute('insert ignore into acfun_complex(aid, title, dateinfo) values (%s, %s, %s)',
            ('3304929', '他们用一张张照片诉说着“全球变暖”', now))
conn.commit()

cur.close()
conn.close()