__author__ = 'maesleung'
# -*- coding:utf-8 -*-
from datetime import datetime
from bs4 import BeautifulSoup

import urllib.request,urllib.parse
import re
import http.cookiejar
import mysql.connector

def getHtml(url):
    cj = http.cookiejar.CookieJar()
    opener = urllib.request.build_opener(urllib.request.HTTPCookieProcessor(cj))
    opener.addheaders = [('User-Agent',
                          'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36'),
                         ('Cookie', '2c7505bca2e54d1e85df92d947f2cc5a')]
    urllib.request.install_opener(opener)
    html_bytes = urllib.request.urlopen(url).read()
    html_string = html_bytes.decode('utf-8')
    return html_string

#综合：        http://www.acfun.tv/v/list110/index.htm
#工作·情感：  http://www.acfun.tv/v/list73/index.htm
#动画文化：    http://www.acfun.tv/v/list74/index.htm
#漫画·轻小说：http://www.acfun.tv/v/list75/index.htm
#游戏：        http://www.acfun.tv/v/list74/index.htm
tarUrl = "http://www.acfun.tv/v/list110/index.htm"
html_doc = getHtml(tarUrl)
soup = BeautifulSoup(html_doc, 'html.parser')

#建立连接对象
conn = mysql.connector.connect(user="root", password='mysql', database='acfun_db',charset='utf8')
cur = conn.cursor()

now = datetime.now()
now = now.strftime('%y%m%d%H')
block = soup.find_all('div', attrs = {"class" : "block"})

for b in block:
    if '今日最热' in str(b):
        for a in b.find_all('a'):
            cur.execute('insert into acfun_complex(aid, title, dateinfo) values (%s, %s, %s)',
                        [a['data-aid'], a['title'], now])
            conn.commit()
cur.close()
conn.close()
